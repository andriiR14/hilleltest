package org.andrii.hillel;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

public class FileUtil {
	
	public static Set<String> getHashSet(File file, String ignoreChars) throws IOException {
		
		Set<String> hashSet = new HashSet<>();
		
		try (LineIterator it = FileUtils.lineIterator(file, "UTF-8")) {
			while (it.hasNext()) {
		        String line = it.nextLine();
		        String[] words = line.replaceAll("[" + ignoreChars + "]", "").toLowerCase().split("\\s+");
		        hashSet.addAll(Arrays.asList(words));
		    }
		}
		
		return hashSet;
	}
}
