package org.andrii.hillel;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

public class IncrementSynchronize {

    private int value = 0;
    
    public synchronized int getNextValue() {
    	return value++;
    }
    
    public int getNextValue4() {
    	synchronized(this) {
    		value++;
		}
    	return value;
    }
    
    ReentrantLock lock = new ReentrantLock();
    public int getNextValue2() {
    	lock.lock();
    	try {
    		value++;
    	} finally {
    		lock.unlock();
    	}
    	
    	return value;
    }
    
    Semaphore semaphore = new Semaphore(1);
    public int getNextValue3() {
    	
    	try {
    		semaphore.acquire();
			value++;
    	} catch(InterruptedException ex) {
    		ex.printStackTrace();
    	} 
    	finally {
    		semaphore.release();
    	}
    	
    	return value;
    }
}
