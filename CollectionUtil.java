package org.andrii.hillel;

import java.util.Arrays;
import java.util.Collection;

public class CollectionUtil {
	
	public static <T> void addArrayToCollection(T[] arr, Collection<T> coll) {
		
		for (T a : arr) {
			coll.add(a);
		}
	}
	
	public static <T> void addArrayToCollection2(T[] arr, Collection<T> coll) {
		
		coll.addAll(Arrays.asList(arr));
	}

}
